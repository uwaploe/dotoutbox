// Dotoutbox manages the DOT Buoy outbox on the server
package main

import (
	"crypto/x509"
	"encoding/pem"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"text/template"

	"github.com/pkg/errors"
	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/terminal"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `Usage: dotoutbox [options] cmd [file]

Manage the DOT Buoy outbox. Valid values of "cmd" are:

    list - list the outbox contents
    del file - delete a file from the outbox
    add file - add a file to the outbox
`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	dotServer, dotId string
	sftpKeyfile      string
)

const LS_TEMPLATE = "{{.Size}}\t{{.ModTime}}\t{{.Name}}"

func lookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func getPassword(prompt string) ([]byte, error) {
	fd := int(os.Stdin.Fd())
	state, err := terminal.GetState(fd)
	if err != nil {
		return nil, err
	}

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Interrupt, os.Kill)
	defer signal.Stop(ch)

	go func() {
		_, ok := <-ch
		if ok {
			terminal.Restore(fd, state)
			os.Exit(1)
		}
	}()

	fmt.Print(prompt)
	pass, err := terminal.ReadPassword(fd)
	fmt.Println("")

	return pass, err
}

func readPrivateKey(filename string) (ssh.Signer, error) {
	contents, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, errors.Wrap(err, "read key file")
	}

	block, _ := pem.Decode(contents)

	if x509.IsEncryptedPEMBlock(block) {
		fmt.Printf("%s is password protected\n", filename)
		pass, err := getPassword("Enter Password: ")
		if err != nil {
			return nil, errors.Wrap(err, "read password")
		}
		return ssh.ParsePrivateKeyWithPassphrase(contents, pass)
	}

	return ssh.ParsePrivateKey(contents)
}

func pubkeyConnect(user, host, keyFile string) (*ssh.Client, error) {
	signer, err := readPrivateKey(keyFile)
	if err != nil {
		return nil, errors.Wrap(err, "parse key file")
	}

	config := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			// Use the PublicKeys method for remote authentication.
			ssh.PublicKeys(signer),
		},
		// Host authentication is not needed for this application
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	return ssh.Dial("tcp", host, config)
}

func listOutbox(c *sftp.Client, id string) ([]os.FileInfo, error) {
	return c.ReadDir(fmt.Sprintf("/home/dot/%s/OUTBOX", id))
}

func addFile(c *sftp.Client, id, path string) (int64, error) {
	dir := fmt.Sprintf("/home/dot/%s/OUTBOX", id)

	fin, err := os.Open(path)
	if err != nil {
		return 0, err
	}
	defer fin.Close()

	filename := filepath.Base(path)
	fout, err := c.Create(c.Join(dir, filename))
	if err != nil {
		return 0, err
	}
	defer fout.Close()

	return fout.ReadFrom(fin)
}

func rmFile(c *sftp.Client, id, path string) error {
	dir := fmt.Sprintf("/home/dot/%s/OUTBOX", id)
	rpath := c.Join(dir, filepath.Base(path))
	return c.Remove(rpath)
}

func showListing(w io.Writer, tmpl *template.Template, fi []os.FileInfo) {
	if len(fi) == 0 {
		w.Write([]byte("No entries\n"))
		return
	}

	for _, e := range fi {
		tmpl.Execute(w, e)
		w.Write([]byte("\n"))
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}
	flag.StringVar(&dotId, "id", lookupEnvOrString("DOT_ID", dotId), "DOT Buoy ID")
	flag.StringVar(&sftpKeyfile, "key", lookupEnvOrString("DOT_SSHKEY", sftpKeyfile),
		"SSH private key file")
	flag.StringVar(&dotServer, "host", lookupEnvOrString("DOT_HOST", dotServer),
		"DOT server host")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	if dotId == "" {
		log.Fatal("Buoy ID must be specified (use --id option)")
	}

	conn, err := pubkeyConnect("dot", dotServer, sftpKeyfile)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	c, err := sftp.NewClient(conn)
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()

	switch args[0] {
	case "add", "put":
		if len(args) < 2 {
			log.Fatalf("Missing argument to %q command", args[0])
		}
		for _, arg := range args[1:] {
			if n, err := addFile(c, dotId, arg); err != nil {
				log.Printf("Cannot upload %q: %v", arg, err)
			} else {
				log.Printf("Uploaded %s (%d bytes)", arg, n)
			}
		}
	case "rm", "del":
		if len(args) < 2 {
			log.Fatalf("Missing argument to %q command", args[0])
		}
		for _, arg := range args[1:] {
			if err = rmFile(c, dotId, arg); err != nil {
				log.Printf("Cannot remove %q: %v", arg, err)
			}
		}
	case "list", "ls", "dir":
		tmpl := template.Must(template.New("ls").Parse(LS_TEMPLATE))
		fi, err := listOutbox(c, dotId)
		if err != nil {
			log.Fatal(err)
		}
		showListing(os.Stdout, tmpl, fi)
	default:
		log.Fatalf("Unknown command: %q", args[0])
	}

}
