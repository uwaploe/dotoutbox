module bitbucket.org/uwaploe/dotoutbox

require (
	github.com/pkg/errors v0.8.1
	github.com/pkg/sftp v1.10.1
	golang.org/x/crypto v0.0.0-20190911031432-227b76d455e7
)
